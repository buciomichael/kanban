import './App.css';
import { Box, Flex, Text } from '@chakra-ui/react';
import { DndContext, KeyboardSensor, PointerSensor, closestCenter, closestCorners, useSensor, useSensors } from '@dnd-kit/core';
import { SortableContext, arrayMove, sortableKeyboardCoordinates } from '@dnd-kit/sortable';
import { useState } from 'react';
import Container from './Components/Container';
import Items from './Components/Items';
import { containerData } from './Components/Data';

function App() {

  const [containers, setContainers] = useState(containerData);
  const [activeId, setActiveId] = useState(null);
  const [currentContainerId, setCurrentContainerId] = useState(null)
  // DND Handlers
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  );

  const handleDragStart = (event) => {
    const { active } = event;
    const id = active.id;
    setActiveId(id);
  }

  const handleDragOver = (event) => {
    const { active, over } = event;
    console.log('ACTIVE', active)
    console.log('OVER', over)
    if (
      active &&
      over &&
      active.id.toString().includes('item') &&
      over?.id.toString().includes('container') &&
      active.id !== over.id
    ) {
        // Find the active and over container
        const activeContainer = containers.find((container) => {
          return container.items.find((item) => item.id === active.id);
        })
        const overContainer = containers.find((container) => {
          return container.id === over.id
        })
        // If the active or over container is undefined, return
        if (!activeContainer || !overContainer) return;

        // find the active and over container by index
        const activeContainerIndex = containers.findIndex((container) => container.id === activeContainer.id);
        const overContainerIndex = containers.findIndex((container) => container.id === overContainer.id);

        // find the index of the active item in the active container
        const activeItemIndex = activeContainer.items.findIndex((item) => item.id === active.id);

        // Remove the active item from the active container and add it to the over container
        let newItems = [...containers];
        const [removedItem] = newItems[activeContainerIndex].items.splice(
          activeItemIndex,
          1
        );
        newItems[overContainerIndex].items.push(removedItem);
        setContainers(newItems);
    }
  }

  const handleDragMove = (event) => {
    const { active, over } = event;
    // handle individual items being sorted
    if (
      active &&
      over &&
      active.id.toString().includes('item') &&
      over?.id.toString().includes('item') &&
      active.id !== over.id
    ) {
      // find the active container and the over container of the moving item
      const activeContainer = containers.find((container) => {
        return container.items.find((item) => item.id === active.id);
      })
      const overContainer = containers.find((container) => {
        return container.items.find((item) => item.id === over.id);
      })
      // If the active or over container is undefined, return
      if (!activeContainer || !overContainer) return;

      // find the active and over container by index
      const activeContainerIndex = containers.findIndex((container) => container.id === activeContainer.id);
      const overContainerIndex = containers.findIndex((container) => container.id === overContainer.id);
      // find the active and over item by index
      const activeItemIndex = activeContainer.items.findIndex((item) => item.id === active.id);
      const overItemIndex = overContainer.items.findIndex((item) => item.id === over.id);

      // if the item moved is dropped in the same container
      if (activeContainerIndex === overContainerIndex) {
        let newItems = [...containers];
        newItems[activeContainerIndex].items = arrayMove(
          newItems[activeContainerIndex].items,
          activeItemIndex,
          overItemIndex,
        );
        setContainers(newItems);
      } else {
        // if the item moved is in a different container
        let newItems = [...containers];
        const [removedItem] = newItems[activeContainerIndex].items.splice(
          activeItemIndex,
          1,
        );
        newItems[overContainerIndex].items.splice(
          overItemIndex,
          0,
          removedItem,
        );
        setContainers(newItems);
      }
    }
  }

  const handleDragEnd = (event) => {
    const { active, over } = event;
    console.log('OVER DRAG END', over)
    // Handle Container Sorting
    if(
      active &&
      over &&
      active.id.toString().includes('container') &&
      over?.id.toString().includes('container') &&
      active.id !== over.id
      ) {
        const activeContainerIndex = containerData.findIndex(
          (container) => container.id === active.id
        );
        const overContainerIndex = containerData.findIndex(
          (container) => container.id === over.id
        );
        // swap the active and over container
        let newItems = [...containers];
        newItems = arrayMove(newItems, activeContainerIndex, overContainerIndex);
        setContainers(newItems);
      }

      // Handle Item Sorting
      if(
        active &&
        over &&
        active.id.toString().includes('item') &&
        over?.id.toString().includes('item') &&
        active.id !== over.id
        ) {
        // Find the active and over container
        const activeContainer = containers.find((container) => {
          return container.items.find((item) => item.id === active.id);
        })
        const overContainer = containers.find((container) => {
          return container.items.find((item) => item.id === over.id);
        })
        // If the active or over container is undefined, return
        if (!activeContainer || !overContainer) return;

        // find the active and over container by index
        const activeContainerIndex = containers.findIndex((container) => container.id === activeContainer.id);
        const overContainerIndex = containers.findIndex((container) => container.id === overContainer.id);

        // find the index of the active item & over item
        const activeItemIndex = activeContainer.items.findIndex((item) => item.id === active.id);
        const overItemIndex = activeContainer.items.findIndex((item) => item.id === over.id);

        // In the same container
        if (activeContainerIndex === overContainerIndex) {
          let newItems = [...containers];
          newItems[activeContainerIndex].items = arrayMove(
            newItems[activeContainerIndex].items,
            activeItemIndex,
            overItemIndex
          );
          setContainers(newItems);
        } else {
          // In different container
          let newItems = [...containers];
          const [removedItem] = newItems[activeContainerIndex].items.splice(
            activeItemIndex,
            1
          );
          newItems[overContainerIndex].items.splice(
            overItemIndex,
            0,
            removedItem
          );
          setContainers(newItems);
        }
      }
  }

  return (
    <Box>
      <Flex
      direction='column'
      align='center'
      >
      <Text>DND Kit Practice</Text>
      <Box
        border='1px solid black'
        h='70vh'
        w='90vw'
        >
      <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragMove={handleDragMove}
      onDragEnd={handleDragEnd}
      onDragOver={handleDragOver}
      >
        <SortableContext items={containers.map((i) => i.id)}>
          <Flex
          m={5}
          gap={5}
          h='100%'
          >
          {containers.map((container) => (
            <Container key={container.id} id={container.id} container={container}>
              <SortableContext
              items={container.items.map((i) => i.id)}
              >
                {container.items.map((item) => (
                  <Items id={item.id} key={item.id} item={item} />
                ))}
              </SortableContext>
            </Container>
          ))}
          </Flex>
        </SortableContext>
      </DndContext>
        </Box>
      </Flex>
    </Box>
  );
}

export default App;
