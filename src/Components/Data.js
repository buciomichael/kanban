export const containerData = [
        {
          id: 'container-1',
          title: 'Container 1',
          items: [
            { id: 'item-1', title: 'Go to the gym' },
            { id: 'item-2', title: 'Walk the dog' }
          ]
        },
        {
          id: 'container-2',
          title: 'Container 2',
          items: [
            { id: 'item-3', title: 'Pay bills' },
            { id: 'item-4', title: 'Go shopping' }
          ]
        }
]
