import { Box, Button, Flex, Spacer, Text } from '@chakra-ui/react'
import {useRef, React} from 'react'
import {useSortable} from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import {useDroppable} from '@dnd-kit/core';

export default function Container({container, children}) {
  const {
    attributes,
    listeners,
    setNodeRef: sortableSetNodeRef,
    transform,
    transition,
    isDragging
  } = useSortable({
    id: container.id,
  });


  const droppableRef = useDroppable({
    id: container.id, // Make sure 'props' is defined or replace it with the appropriate variable
  });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
    opacity: isDragging ? 0.5 : 1, // Set opacity based on isDragging
  };


  return (
    <Box
    ref={(el) => {
      sortableSetNodeRef(el); // Set the sortable ref
      droppableRef.setNodeRef(el); // Set the droppable ref
    }}
    style={style}
    {...attributes}
    border='1px solid black'
    h='80%'
    w='350px'
    >
      <Flex
      m={2}
      align='center'
      >
        <Text
        fontSize='2em'
        fontWeight={600}
        >
          {container.title}
        </Text>
        <Spacer />
        <Box>
          <Button
          {...listeners}
          >
            Drag Handle
          </Button>
        </Box>
      </Flex>
      {children}
    </Box>
  )
}
