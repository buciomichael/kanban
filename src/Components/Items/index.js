import { Box, Button } from '@chakra-ui/react'
import { useDraggable } from '@dnd-kit/core';
import { useSortable } from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import React from 'react'

export default function Items({item}) {
    const {
        attributes,
        listeners,
        setNodeRef,
        transform,
        transition,
        isDragging,
      } = useSortable({id: item.id});

      return (
        <Box
          ref={setNodeRef}
          {...attributes}
          style={{
            transition,
            transform: CSS.Transform.toString(transform),
            opacity: isDragging ? 0.5 : 1, // Set opacity based on isDragging
          }}
          {...listeners}
        >
          <Box m={5}>
            {item.title}
          </Box>
        </Box>
      );
}
